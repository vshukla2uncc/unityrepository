# EventVisualization
Does visualization of music events (in country(USA) and category, hardcoded for now) using unity3d.

This is a visualization application made using unity 3d to visualize data.

To launch this in Unity3d follow the steps below:

1. Download the folder EventVisualization-master from this repository.
2. Open Unity3d. Select open new project.
3. Select the project folder EventVisualization-master.
4. Select yes for updating project files to latest version (if prompted).
5. Once the project is launched in the project tab select assests (on screen).
6. Double click the eart scene to open it. The Earth scene has a unity icon and is titled "Earth".
7. Once the scene is open. Click play and the game view will open the visualization.
8. Use the keys mentioned at the corners of the screen to interact with the visualization.
9. Once done. Again click on the play icon and the visualization will stop and unity will return to the scene view.
10. Do explore all aspects of the visualization and also all the files of the project to have a better understanding of the project

Source: GitHub

# README #

This repository contains applications made in Unity3d.

* This repository contains visualizations made using unity3d. It also contains basic VR games made for oculus using unity3d.
* Version 1
* Repo link: https://vshukla2uncc@bitbucket.org/vshukla2uncc/unityrepository.git

* To use the files on this repository you will need to install Unity3d.
* The latest Unity3d software on a windows or mac machine is recommended.
* Make sure you atleast have 4gb of ram on your machine to run these applications.
* To run the different projects in this repository, download them by folder name individually and then run them by reading the Readme file present inside each of the respective project folder.
* Each folder corresponds to a different project/application. The project or application's readme file is present inside the project folder. This file will contain details on how to run the project or application.

* For any questions you can contact Vidhan Shukla or Dr. Aidong Lu.
* Please contact via Email to vshukla2@uncc.edu.
# A-Perfect-Find
Unity / Oculus game

Basic oculus virtual reality game made using unity3d.

This is a basic game made using unity 3d for oculus rift.

This game has been shared for you to explore the files and structure involved in creating a basic VR game for oculus.

1. To launch this in Unity3d follow the steps below:
2. Download the folder "The Perfect Find - Oculus" from this repository.
3. Open Unity3d. Select open new project.
4. Select the project folder "The Perfect Find - Oculus".
5. Select yes for updating project files to latest version (if prompted).
6. Once the project is launched, go to Projects -> settings -> build and click on Build.
7. Once the build is complete. Go to the project folder and click on the .exe file that has been created.
9. The game will launch. Use the arrow keys and mouse to play the game.
10. Once done. Click close.
11. Do explore all aspects of the game and also all the files of the project to have a better understanding of the project.

Source: GitHub